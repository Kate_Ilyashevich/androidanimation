package com.example.kate.androidanimation;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button startAnim;
    private ImageView animationView;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startAnim = (Button) findViewById(R.id.startAnim);
        animationView = (ImageView) findViewById(R.id.animationView);
        startAnim.setOnClickListener(this);
    }
    public void onClick(View v) {
            animationView.setBackgroundResource(R.drawable.enot);
            Animation transformAnimation =
                AnimationUtils.loadAnimation(this, R.anim.my_animation);
            animationView.startAnimation(transformAnimation);
    }
}
